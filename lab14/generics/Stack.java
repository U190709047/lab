package generics;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.*;

public interface Stack<T> {
    void push(T item);
    T pop();
    boolean empty();

    public List<T> toList();

    default void addAll(Stack<? extends T> aStack) {
        List<? extends T> values = aStack.toList();
        reverse(values);
        for(T value : values){
            push(value);
        }
    }


}
