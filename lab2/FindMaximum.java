public class FindMaximum {

	public static void main(String[] args){

                int value1 = 5;
                int value2 = 7;
                int result;

                boolean someCondition = value1 > value2;

                result = someCondition ? value1 : value2; 

                System.out.println(result);
            
	}
}