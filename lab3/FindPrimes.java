
public class FindPrimes {

    public static void main(String[] args){
        System.out.println("Max = " + args[0]);
        int max = Integer.parseInt(args[0]);

        for (int num = 2; num < max; num++){

            int divisor = 2;
            boolean isPrime = true;
            while ( divisor < num && !isPrime){
                if ( num % divisor == 0) {
                    isPrime = false;
                }
                divisor ++;
            }
            if(isPrime)
                System.out.print(num +",");

        }



    }
}