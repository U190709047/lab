package shapes.lab9.shapes2d;

import shapes2d.Circle;
import shapes2d.Square;

public class TestShapes2d {

    public static void main(String[] args ){
        shapes2d.Circle c = new Circle(5);
        shapes2d.Square s = new Square(4);

        System.out.println("Circle Area = " + c.area());
        System.out.println(c);
        System.out.println(s);

    }
}
