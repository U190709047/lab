package shapes.lab9.shapes3d;

import shapes3d.Cube;
import shapes3d.Cylinder;

public class TestShapes3d {

    public static void main(String[] args ){
        shapes3d.Cylinder cy =new Cylinder(5,10);

        System.out.println(cy);
        System.out.println("Cylinder Area = " + cy.area());
        System.out.println("Cylinder Volume = " + cy.volume());

        shapes3d.Cube cube = new Cube(4);
        System.out.println(cube);
        System.out.println("Cube Volume = " + cube.volume());
        System.out.println("Cube Area = " + cube.area());
    }


}
